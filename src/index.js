/**
 * @flow
 */
import { createStore as reduxCreateStore, combineReducers as reduxCombineReducers, bindActionCreators } from "redux";

let ownKeys;
if (typeof Reflect !== 'object' || typeof Reflect.ownKeys !== 'function') {
    if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = function(o) {
            return Object.getOwnPropertyNames(o).concat(Object.getOwnPropertySymbols(o));
        };
    }
    else {
        ownKeys = Object.getOwnPropertyNames;
    }
}
else {
    ownKeys = Reflect.ownKeys;
}

function isComposited(obj) {
    return typeof obj === 'object' && obj !== null && !(obj instanceof Array) && !(obj.toList); // test for immutable js
}

function nameForSetter(key) {
    return 'set' + key.charAt(0).toUpperCase() + key.slice(1);
}

/**************************** Options ****************************/

const noop = function () { return arguments; };
let defaultOptions = {
    asyncAction: {
        request: noop,
        ok: function (payload) {
            Object.assign(this, payload);
        },
        error: noop
    },
};

function mapOptions(options) {
    ownKeys(options).forEach(key => {
        // fast (and dangerous) check if object is plain
        const value = options[key];
        if (value && value.constructor === Object) {
            options[key] = function (arg) { Object.assign(this, value); return arg };
        }
    });

    return {
        get request() { return options.request || defaultOptions.asyncAction.request },
        get ok() { return options.ok || defaultOptions.asyncAction.ok },
        get error() { return options.error || defaultOptions.asyncAction.error }
    };
}

export function setDefaultOptions(options) {
    Object.assign(defaultOptions.asyncAction, options.asyncAction);
}


/**************************** Redux helpers ****************************/

let store = {getState(){ throw new Error("Store not configured!") }};
const dynamicReducers = {};
const dynamicStorages = {};


function injectReducer(store, name, dynReducer) {
    dynamicReducers[name] = dynReducer;

    if (store && store.dispatch) {
        store.dispatch({ type: "@@"+name+"/INIT"})
    }
}

export function createStore(reducer, preloadedState, enhancer) {
    if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
        enhancer = preloadedState;
        preloadedState = undefined;
    }
    store = reduxCreateStore(rootReducer(reducer, preloadedState), preloadedState, enhancer);
    return store;
}

function rootReducer(reducer, preloadedState) {
    return function(state = preloadedState, action) {
        let hasChanged = false;
        let nextState = Object.assign({}, state);

        // External reducer
        if (reducer) {
            let shapedState = Object.assign({}, state);
            if (process.env.NODE_ENV !== 'production') {
                // prevent `Unexpected keys...` warning
                ownKeys(dynamicReducers).forEach(key => delete shapedState[key]);
            }

            const theirsState = reducer(shapedState, action);

            if (theirsState !== shapedState) {
                hasChanged = true;
                nextState = Object.assign(nextState, theirsState);
            }
        }

        if (action.type.indexOf('@@') === 0) {
            // conventional, service action
            ownKeys(dynamicReducers).forEach(key => {
                const dynReducer = dynamicReducers[key];
                const oursState = dynReducer ? dynReducer(state[key], action) : state[key];

                if (oursState !== state[key]) {
                    hasChanged = true;
                    Object.assign(nextState, { [key]: oursState });
                }
            });
        }
        else {
            const storageName = action.type.split('.')[0];
            const dynReducer = dynamicReducers[storageName];
            const oursState = dynReducer ? dynReducer(state[storageName], action) : state[storageName];

            if (oursState !== state[storageName]) {
                hasChanged = true;
                nextState = Object.assign(nextState, { [storageName]: oursState });
            }
        }

        return hasChanged ? nextState : state;
    };
}

function stateProvider(path) {
    let i, len, state = store.getState();

    for (i = 0, path = path.split('.'), len = path.length; i < len; i++) {
        if (!state || typeof state !== 'object') return undefined;

        state = state[path[i]];
    }

    return Object.assign({}, state);
}


/**************************** CORE ****************************/

function buildReducer(obj, storeName) {
    function makeInitialState(object, initialState = {}) {
        Object.getOwnPropertyNames(object).forEach(key => {
            if (typeof object[key] === 'function') {
                return;
            }
            else if (isComposited(object[key])) {
                // inheritance && composition
                if (console && console.warn) {
                    const descr = Object.getOwnPropertyDescriptor(object, key);
                    let setter = nameForSetter(key);
                    if (typeof object[setter] !== 'function') {
                        console.warn("For correct work with composite objects, you need to specify a setter `" + setter + "` in " + object.constructor.name);
                    }
                }
                initialState[key] = makeInitialState(object[key]);
            }
            else if (!object.__transient || !!object.__transient.indexOf(key)) {
                initialState[key] = object[key];
            }
        });
        return initialState;
    }
    const initialState = makeInitialState(obj);
    const targetType = storeName + '.';

    return function (state = initialState, action) {
        if (action.type.indexOf(targetType) !== 0) {
            return state; // that`s not mine!
        }

        const path = action.type.split('.');
        const payload = Object(action.payload);
        let nextState = Object.assign({}, state);

        let partialUpdate = nextState;
        for (let i = 1; i < path.length -1; i++) {
            partialUpdate = partialUpdate[path[i]];
        }

        Object.keys(partialUpdate).forEach(key => {
            if (payload.hasOwnProperty(key)) {
                partialUpdate[key] = payload[key];
            }
        });

        return nextState;
    }
}

function wrapClass(Clazz, storeName) {
    Clazz.__storeName = storeName;
    let wrapper = dynamicStorages[storeName];

    if (!wrapper) {
        // mix into the class core functions from store - just for fun
        wrapper = dynamicStorages[storeName] = Clazz;
        Object.defineProperty(Clazz.prototype, "getState", { get: function() { return store.getState } });
        Object.defineProperty(Clazz.prototype, "dispatch", { get: function() { return store.dispatch } });

        // make statics, can be called as `Class.someAction(args)`
        Object.getOwnPropertyNames(Clazz).forEach(prop => {
            if (Clazz[prop] && Clazz[prop].type) {
                wrapper[prop] = function(...args) {
                    return new wrapper()[prop](...args);
                }
            }
        });

        // get current storage state
        wrapper.getState = stateProvider.bind(wrapper, storeName);
    }

    return wrapper;
}

function execWithState(func, ...params) {
    // allow static methods?
    let context = this;
    if (typeof this === 'function') {
        try { context = new this() }
        catch (e) { console.warn("Couldn't initialize context for Action") }
    }

    const initialState = context.constructor.getState();
    Object.assign(context, initialState);
    Object.getOwnPropertyNames(context).forEach(key => {
        if (isComposited(context[key])) {
            // use setter, if exists
            let setter = nameForSetter(key);
            if (typeof context[setter] === 'function') {
                context[setter](initialState[key]);
            }
        }
    });

    const result = func.apply(context, params);

    let payload = {};
    Object.keys(initialState).forEach(key => {
        if (initialState[key] !== context[key]) {
            payload[key] = context[key];
        }
    });

    return { payload, result };
}


/**************************** DECORATORS ****************************/

/**
 * Decorator. Make reducer, inject it into redux store. Provide properties for state and create initial state.
 * Also mix into the class static actions and static `getState()` - returns current redux state for that reducer.
 * Add to the instance object methods `getState()`, `dispatch()` - from redux store.
 * @param target
 * @param name
 * @param descriptor
 * @returns {*}
 * @constructor
 */
export function Storage() {
    function make(storeName, Clazz) {
        injectReducer(store, storeName, buildReducer(new Clazz(), storeName));
        return wrapClass(Clazz, storeName);
    }

    if (typeof arguments[0] !== 'function') {
        return make.bind(null, arguments[0]);
    }
    return make(arguments[0].name, arguments[0]);
}

/**
 * Decorator. Mark class method as plain action.
 * @param target
 * @param name
 * @param descriptor
 * @returns {*}
 * @constructor
 */
export function Action(target, name, descriptor) {
    function action(...args) {
        let { payload, result } = execWithState.call(this, impl, ...args);
        const actionType = (this.__storeName || this.constructor.__storeName) + '.' + name;

        store.dispatch({
            type: actionType,
            payload
        });

        return result;
    }

    const impl = descriptor.value;

    const targetConstructor = target.constructor.name === 'Function' ? target : target.constructor;
    descriptor.value = targetConstructor[name] = action;
    descriptor.writable = false;

    return descriptor;
}

/**
 * Decorator. Mark class method as async action.
 * Any changes to `this` will be ignored, only `Promise.resolve`
 * result will be used to update redux state.
 * @param target
 * @param name
 * @param descriptor
 * @returns {*}
 * @constructor
 */
export function AsyncAction(targetOrOptions, name, descriptor) {
    function make(target, name, descriptor) {
        function action(...args) {
            let context = this;
            if (typeof this === 'function') {
                try { context = new this() }
                catch (e) { console.warn("Couldn't initialize context for Action") }
            }
            const actionType = (context.__storeName || context.constructor.__storeName) + '.' + name;

            const { payload } = execWithState.call(context, options.request, ...args);

            // before action
            store.dispatch({ type: actionType + 'Request', payload, ...args });

            // exec action
            let feature = impl.apply(context, args);
            if (!feature || !feature.then) {
                throw new Error("Expected return type of Promise, but receive ", feature);
            }

            feature // resolve promise
                .then(response => {
                    let { payload, result } = execWithState.call(context, options.ok, response);
                    store.dispatch({ type: actionType + 'Ok', payload });
                    return result || response;
                })
                .catch(error => {
                    let { payload, result } = execWithState.call(context, options.error, error);
                    store.dispatch({ type: actionType + 'Error', payload });
                    return result || error;
                });

            return feature;
        }

        const impl = descriptor.value;

        const targetConstructor = target.constructor.name === 'Function' ? target : target.constructor;
        descriptor.value = targetConstructor[name] = action;
        descriptor.writable = false;

        return descriptor;
    }

    let options = defaultOptions.asyncAction;

    if (typeof name === 'string') {
        return make(targetOrOptions, name, descriptor);
    }

    options = mapOptions(targetOrOptions);
    return make;
}

/**
 * Decorator. Mark property as transient - it shall not pass to redux store
 * @param target
 * @param name
 * @param descriptor
 * @returns {*}
 * @constructor
 */
export function Transient(target, name, descriptor) {
    if (!target.__transient) {
        target.__transient = [];
    }
    target.__transient.push(name);
    return descriptor;
}

/**
 * Simple helper. Points on self class method
 * @param methodName {string}
 * @returns {Function} that should call target method
 */
export function selector(methodName) {
    return function () {
        const func = this[methodName] || new this()[methodName];
        func.apply(this, arguments);
    };
}
